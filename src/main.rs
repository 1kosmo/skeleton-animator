mod viewer;

use viewer::Viewer;

fn main() {
    let mut viewer = Viewer::new(1280, 720);
    viewer.execute();
}
