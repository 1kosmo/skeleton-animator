////////// 3rd Party //////////
use viewer::cgmath::Matrix4;
use viewer::cgmath::Matrix2;
use viewer::cgmath::Vector2;
use viewer::cgmath::SquareMatrix;

////////// STD Lib //////////
use std::collections::HashMap;
use std::collections::BTreeSet;
use std::f32;

////////// My Libs //////////
use viewer::object::ObjectRaw;
use viewer::object::Object;
use viewer::object::ManipulationArea;
mod selected_object;
use self::selected_object::SelectedObject;
use viewer::texture::{Texture_ID, INVALID_TEXTURE};

// TODO: Use ObjectID instead of u32
pub type ObjectID = u32;

pub struct ObjectController {
    objects: HashMap<u32, Object>,
    objects_raw: HashMap<u32, ObjectRaw>,
    object_colors: HashMap<u32, [f32; 4]>,
    object_textures: HashMap<u32, Texture_ID>,
    object_children: HashMap<u32, Vec<u32>>,
    object_parents: HashMap<u32, u32>,

    objects_to_be_manipulated: BTreeSet<u32>,
    
    updated: bool,
    ui_ids: u32,
    id_counter: u32,

    z_interval: f32,
    z_order: Vec<u32>,

    extra_object_margin: f32,

    selected_obj: SelectedObject,
} 


impl ObjectController {
    pub fn new(id_start: u32) -> ObjectController {
        ObjectController {
            objects: HashMap::new(),
            objects_raw: HashMap::new(),
            object_colors: HashMap::new(),
            object_textures: HashMap::new(),
            object_children: HashMap::new(),
            object_parents: HashMap::new(),

            objects_to_be_manipulated: BTreeSet::new(),
            
            updated: false,
            ui_ids: id_start,
            id_counter: id_start,

            z_interval: 0.1,
            z_order: Vec::new(),

            extra_object_margin: 5.0,

            selected_obj: SelectedObject::new(0, ManipulationArea::None),
        }
    }

    // *************** Administrative functions ***************
    pub fn add_specific_object(&mut self, parent: u32, id: u32,
                               x_pos: f32, y_pos: f32, z_pos: f32,
                               width: f32, height: f32,
                               color: [f32; 4], texture: Texture_ID,
                               to_be_manipulated: bool)
    {
        let to_add = ObjectRaw::new(x_pos, y_pos, z_pos, self.z_order.len(),
                                    width, height);
        self.objects_raw.insert(id, to_add);
        self.object_colors.insert(id, color);
        self.object_textures.insert(id, texture);
        self.object_parents.insert(id, parent);
        if parent != 0 { // Update parent vector
            self.object_children.get_mut(&parent).expect(
                "Error: When creating object, given parent of object was not found."
            ).push(id);
        }
        self.object_children.insert(id, Vec::new());
        self.updated = true;
        if to_be_manipulated
        {
            self.objects_to_be_manipulated.insert(id);
        }
    }

    
    pub fn add_object(&mut self, parent: u32, x_pos: f32, y_pos: f32)
                      -> u32
    {
        let new_id = self.id_counter;
        // If only one interval space left, expand
        if self.z_order.len() == (f32::round(0.9 / self.z_interval) - 1.0) as usize
        {
            self.z_interval = self.z_interval / 2.0;
            let mut i = 0;
            for obj in &self.z_order
            {
                let object = self.objects_raw.get_mut(obj).unwrap();
                object.z_pos = i as f32 * self.z_interval;
                i += 1;
            }
        }
        self.z_order.push(new_id); // Insert to be at the top of all objects
        let z_pos = self.z_interval * self.z_order.len() as f32;
        self.add_specific_object(parent, new_id, x_pos, y_pos, z_pos,
                                 1.0, 1.0, [0.0, 0.0, 0.0, 1.0],
                                 INVALID_TEXTURE, true);
        self.augment_counter();
        new_id
    }

    /// This function is meant for auto-generated objects.
    /// It does not bother to put in the z_interval,
    /// and gives an option to disable manipulation on them
    pub fn add_generated_object(&mut self, parent: u32,
                                x_pos: f32, y_pos: f32, z_pos: f32,
                                width: f32, height: f32,
                                texture: Texture_ID, moveable: bool) -> u32
    {
        let new_id = self.id_counter;

        self.add_specific_object(parent, new_id, x_pos, y_pos, z_pos,
                                 width, height, [0.0, 0.0, 0.0, 0.0],
                                 texture, moveable);

        self.augment_counter();
        new_id
    }


    pub fn draw_object(&mut self, parent: u32, x_pos: f32, y_pos: f32)
    {
        let id = self.add_object(parent, x_pos, y_pos);
        self.selected_obj = SelectedObject::new(id, ManipulationArea::SouthEast);
    }


    fn mark_updated(&mut self, id: &u32) {
        self.updated = true;
        self.mark_updated_recurse_helper(id);
    }


    fn mark_updated_recurse_helper(&mut self, id: &u32) {
        {
            let obj = self.objects_raw.get_mut(id).unwrap();
            obj.updated = true;
        }
        for child in self.object_children[id].clone() {
            self.mark_updated(&child);
        }
    }
    

    pub fn update(&mut self, id: &u32, width: f32, height: f32,
                  x_pos: f32, y_pos: f32) {
        {
            let parent = self.object_parents.get(id).expect(
                "Tried to update object, but no object with matching id found");
            let position_transform =
                self.get_object_transform_basis(parent).invert().unwrap();

            let obj = self.objects_raw.get_mut(id).expect(
                "Tried to update object, but no object with matching id found");
            obj.width = width;
            obj.height = height;        
            let pos = position_transform *
                Vector2::new(x_pos - obj.x_pos, y_pos - obj.y_pos);
            obj.x_pos += pos.x;
            obj.y_pos += pos.y;
        }
        self.mark_updated(id);
    }


    pub fn set_obj_attributes(&mut self, id: &u32, width: f32, height: f32,
                               x_pos: f32, y_pos: f32) {
        {
            let obj = self.objects_raw.get_mut(id).expect(
                "Tried to update object, but no object with matching id found");
            obj.width = width;
            obj.height = height;        
            obj.x_pos = x_pos;
            obj.y_pos = y_pos;
        }
        self.mark_updated(id);
    }


    pub fn update_dimensions(&mut self, id: &u32, width: f32, height: f32) {
        {
            let to_update = self.objects_raw.get_mut(id).expect(
                "Tried to update object, but no object with matching id found");
            to_update.width = width;
            to_update.height = height;
        }
        self.mark_updated(id);
    }


    pub fn update_position(&mut self, id: &u32, x_move: f32, y_move: f32) {
        {
            let parent = self.object_parents.get(id).expect(
                "Tried to update object, but no object with matching id found");
            let transformed_move =
                self.get_object_transform_basis(parent).invert().unwrap() *
                Vector2::new(x_move, y_move);

            let obj = self.objects_raw.get_mut(id).expect(
                "Tried to update object, but no object with matching id found");
            // obj.x_pos += x_pos - obj.x_pos;
            // obj.y_pos += y_pos - obj.y_pos;
            obj.x_pos += transformed_move.x;
            obj.y_pos += transformed_move.y;
        }
        self.mark_updated(id);
    }


    pub fn update_rotation(&mut self, id: &u32, rotation: f32) {
        {
            let obj = self.objects_raw.get_mut(id).expect(
                "Tried to update object, but no object with matching id found");
            obj.rotation = rotation;
        }
        self.mark_updated(id);
    }


    pub fn update_x_axis(&mut self, id: &u32, width: f32, x_pos: f32) {
        self.update_position(id, x_pos, 0.0);
        {
            let obj = self.objects_raw.get_mut(id).expect(
                "Tried to update object, but no object with matching id found");
            // obj.x_pos += x_pos - obj.x_pos;
            obj.width = width;
        }
        self.mark_updated(id);
    }


    pub fn update_y_axis(&mut self, id: &u32, height: f32, y_pos: f32) {
        self.update_position(id, 0.0, y_pos);
        {
            let obj = self.objects_raw.get_mut(id).expect(
                "Tried to update object, but no object with matching id found");
            obj.y_pos += y_pos - obj.y_pos;
            obj.height = height;
        }
        self.mark_updated(id);
    }

   
    pub fn change_z_pos(&mut self, id: &u32, direction: i8) {       
        let cur_index = self.objects_raw[id].z_order;
        let mut new_index: usize = 0;
        // 0 is down. Anything else is up
        if direction > 0 {
            if self.objects_raw[id].z_order < (self.z_order.len() - 1) {
                new_index = cur_index + 1;
            } else {
                return
            }
        } else {
            new_index = cur_index - 1;
        }

        self.z_order.swap(cur_index, new_index);
        {
            let switched_forced = self.objects_raw.get_mut(&self.z_order[cur_index])
                .expect("Tried to update object, but no object with matching id found");
            switched_forced.z_pos = self.z_interval * cur_index as f32;
            switched_forced.z_order = cur_index;
        }
        {
            let switched_requested = self.objects_raw.get_mut(id)
                .expect("Tried to update object, but no object with matching id found");
            switched_requested.z_pos = self.z_interval * new_index as f32;
            switched_requested.z_order = new_index;
        }
        self.updated = true;
    }

    
    /// Sets parent of child, and adds child to list of children of parent.
    pub fn link_objects(&mut self, parent: &u32, child: u32) {
        // If object is already linked, unlink
        self.unlink_objects(&child);

        self.object_parents.insert(child, *parent);
        { 
            let children = self.object_children.get_mut(parent).unwrap();
            children.push(child);
        }

        // Update child coordinates to be relative to parent
        let (parent_x, parent_y) = self.get_object_pos(parent);
        let parent_rot = self.get_abs_rotation(parent);

        let new_basis: Matrix2<f32> = Matrix2::new(
            f32::cos(parent_rot), f32::sin(parent_rot),
            -f32::sin(parent_rot), f32::cos(parent_rot));
        let child = self.objects_raw.get_mut(&child).unwrap();
        let non_rel_pos = Vector2::new(child.x_pos - parent_x,
                                       child.y_pos - parent_y);
        let basis_inv = new_basis.invert().unwrap();
        let rel_pos = basis_inv * non_rel_pos;
        // println!("Linking object:");
        // println!("Parent at: ({:?}, {:?}), with rotation: {:?}", parent_x, parent_y, parent_rot);
        // println!("Child was previously at ({:?}, {:?}), is now at: {:?}", child.x_pos, child.y_pos, rel_pos);
        child.x_pos = rel_pos.x;
        child.y_pos = rel_pos.y;
        child.rotation = child.rotation - parent_rot; 
        child.updated = true;
    }


    /// Function unlinks two linked objects
    /// Note: contains procedures that do not need to be run every time, but
    ///   because this function is run so infrequently relative to runtime it is
    ///   acceptable.
    pub fn unlink_objects(&mut self, id: &u32) {
        // Get absolute position of target object and update relative pos to equal
        let (x, y) = self.get_object_pos(id);
        let obj = self.objects_raw.get_mut(id).expect(
            "Error: Could not find object_raw while unlinking two objects");
        obj.x_pos = x;
        obj.y_pos = y;

        // Remove from parent's child list
        let parent = self.object_parents.get_mut(id).expect(
            "Error: Could not find parent object while unlinking two objects");
        if *parent != 0 {
            let children = self.object_children.get_mut(parent).expect(
                "Error: No children found for parent while unlinking two objects");
            let mut i: usize = 0;
            let mut found = false;
            for child in children.clone() {
                if child == *id {
                    found = true;
                    break
                }
                i += 1;
            }
            if found {
                children.swap_remove(i);
            }
        }
        // Reset parent to nothing
        *parent = 0;
    }



    /// Deletes given id from objects vector, updates parent, and deletes
    /// all children
    pub fn delete_object(&mut self, id: &u32) {        
        let deletion_list = self.get_all_children(id);
        
        // Remove from parent's child list
        self.unlink_objects(id);

        // Delete object+all children from storage maps
        for obj in deletion_list {
            let removed = self.objects_raw.remove(&obj);
            self.objects.remove(&obj);
            self.object_children.remove(&obj);
            self.object_colors.remove(&obj);
            self.object_textures.remove(&obj);
            self.object_parents.remove(&obj);
            self.objects_to_be_manipulated.remove(&obj);
            if removed.is_some() && obj > self.ui_ids {
                self.z_order.remove(removed.unwrap().z_order);
            }
        }
        // ~~~!!!!~~~
        // TODO:
        // When object is deleted, this function should delete children objects.
        // However, in the renderer, they are still being drawn.
        // return deletion_list
    }


    fn get_all_children(&self, id: &u32) -> Vec<u32> {
        let mut list = Vec::new();
        list.push(id.clone());
        for child in &self.object_children[id] {
            list.append(&mut self.get_all_children(child));
        }
        list
    }
    

    fn augment_counter(&mut self) {
        self.id_counter += 1;
    }


    pub fn get_object_params(&mut self) ->
        Vec<(u32, Matrix4<f32>, [f32; 4], Texture_ID)>
    {
        let mut mats = Vec::new();
        if !self.updated {
            return mats
        }

        // let identity: Matrix4<f32> = Matrix4::identity();
        let mut updated_objects: Vec<u32> = Vec::new();
        for (obj_id, obj) in self.objects_raw.iter() {
            if obj.updated {
                updated_objects.push(*obj_id);
            }
        }

        for obj_id in &updated_objects {
            self.apply_branch_transforms(obj_id);
        }

        for obj_id in &updated_objects {
            mats.push((obj_id.clone(),
                       self.objects.get(obj_id).unwrap().rel_transform_mat,
                       *self.object_colors.get(obj_id).unwrap(),
                       *self.object_textures.get(obj_id).unwrap(),
            ));
        }
        
        mats
    }


    /// Recursively applies transformation matrix all objects in branch of tree
    /// that given id is in
    fn apply_branch_transforms(&mut self, id: &u32) {
        let parent_id = 
        {
            let obj = self.objects_raw.get(id).expect(
                "Error: Did not find object when applying mat stacks");
            if !obj.updated {
                return 
            }
            self.object_parents[id]
        };

        if parent_id != 0 {
            let parent_prop_mat = self.get_updated_prop_mat(&parent_id);
            if parent_prop_mat.is_none() {
                // Parent has not been updated yet
                return
            }
            let parent_prop_mat = parent_prop_mat.unwrap();
            let raw = self.objects_raw.get_mut(id).unwrap();
            self.objects.insert(*id, Object::new(raw, parent_prop_mat));
            raw.updated = false;    
        } else {
            let raw = self.objects_raw.get_mut(id).unwrap();
            self.objects.insert(*id, Object::new_at_root(raw));
            raw.updated = false;    
        }

        // Update children 
        for child in self.object_children[id].clone() {
            self.apply_transforms_recursively(&child);
        }
    }


    // Returns prop_mat if parent does not need to be updated. None if it does
    fn get_updated_prop_mat(&mut self, id: &u32) -> Option<Matrix4<f32>> {
        let obj = self.objects_raw.get(id).expect(
            "Error: Tried to get updated prop mat from object");
        if obj.updated {
            None
            // We can send None because we know that it's still in the to-be
            // updated list. The child object that's calling this function
            // will be updated eventually.
        } else {
            Some(self.objects[id].propagation_mat)
            // ~~~!!!~~~ Consider storing update bool separately
        }
    }


    // Function expects to only be called by 'apply_branch_transforms'.
    // Therefore, every parent object is assumed to have up-to-date transforms.
    fn apply_transforms_recursively(&mut self, id: &u32) {
        let parent_id = self.object_parents[id];
        let parent_transform = self.objects.get(&parent_id).expect(
            "Error: Did not find parent object when applying mat stacks")
            .propagation_mat;
        let children = self.object_children[id].clone();
        {
            let raw = self.objects_raw.get_mut(id).unwrap();
            self.objects.insert(*id, Object::new(raw, parent_transform));
            raw.updated = false;
        }

        for child in children {
            self.apply_transforms_recursively(&child);
        }
    }


    pub fn mouse_is_on(&self, x: f32, y: f32) -> (u32, ManipulationArea)
    {
        let mut matching = self.objects_to_be_manipulated.iter().filter(
            |id| {
                let object = self.objects.get(id).unwrap();
                object.mouse_on(x, y)
            }
        );

        let first = matching.next();
        if first.is_none() {
            return (0, ManipulationArea::None)
        }
        let first = first.unwrap();
        let id = matching.fold(
            first,
            |id1, id2| {
                let obj1 = self.objects.get(id1).unwrap();
                let obj2 = self.objects.get(id2).unwrap();
                if obj1.rel_transform_mat.w.z >= obj2.rel_transform_mat.w.z {
                    id1
                } else {
                    id2
                }
            });
        if *id < self.ui_ids {
            (*id, ManipulationArea::UI)
        } else {
            (*id, ManipulationArea::Center)
        }
    }


    pub fn mouse_is_clicked_on(&mut self, x: f32, y: f32)
                               -> (u32, ManipulationArea)
    {
        let (id, area) = self.mouse_is_on(x, y);
        if area != ManipulationArea::UI {
            self.selected_obj.id = id;
            self.selected_obj.manip_area = area;
        }
        (id, area)
    }


    /// Function returns absolute position, factoring in displacement from multiple
    /// levels of parents
    pub fn get_object_pos(&self, id: &u32) -> (f32, f32) {
        let parent = self.object_parents.get(id).expect(
            "ERROR: Attempted to get parent of an object, but no object with given id exists");
        let obj = self.objects_raw.get(id).expect(
            "ERROR: Attempted to get position of an object, but no object with given id exists");
        if *parent == 0 {
            (obj.x_pos, obj.y_pos)
        } else {
            let (parent_x, parent_y) = self.get_object_pos(parent);
            let new_basis = self.get_object_transform_basis(parent);
            let rel_child_pos = Vector2::new(obj.x_pos, obj.y_pos);
            let non_rel_child_pos = new_basis * rel_child_pos;
            (non_rel_child_pos.x + parent_x, non_rel_child_pos.y  + parent_y)
        }
    }


    pub fn get_object_dims(&self, id: &u32) -> (f32, f32) {
        let obj = self.objects_raw.get(id).expect(
            "ERROR: Attempted to get dimensions of an object, but no object with given id exists");
        (obj.height, obj.width)
    }

    
    fn get_abs_rotation(&self, id: &u32) -> f32 {
        let cur = self.objects_raw.get(id).unwrap();
        let parent = self.object_parents.get(id).unwrap();
        if *parent == 0 {
            return cur.rotation
        }
        return cur.rotation + self.get_abs_rotation(parent)
    }


    fn get_object_transform_basis(&self, id: &u32) -> Matrix2<f32> {
        let basis;
        if *id != 0 {
            let parent_rot = self.get_abs_rotation(id);
            basis = Matrix2::new( f32::cos(parent_rot), f32::sin(parent_rot),
                                 -f32::sin(parent_rot), f32::cos(parent_rot));
        } else {
            basis = Matrix2::identity();
        }
        basis
    }
}
