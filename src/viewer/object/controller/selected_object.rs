////////// 3rd Party //////////
use viewer::cgmath::SquareMatrix;
use viewer::cgmath::Vector2;

////////// STD Lib //////////
use std::f32;

////////// My Libs //////////
use viewer::object::ManipulationArea;
use viewer::object::controller::ObjectController;
use viewer::ui;
use viewer::texture::INVALID_TEXTURE;


pub struct SelectedObject {
    pub id: u32,
    pub manip_area: ManipulationArea,
}


impl SelectedObject {
    pub fn new(id: u32, manip_area: ManipulationArea) -> SelectedObject {
        SelectedObject { id: id, manip_area: manip_area }
    }
}


impl ObjectController {
    pub fn adjust_object(&mut self, mouse_x: f32, mouse_y: f32,
                         rel_mouse_x: f32, rel_mouse_y: f32)
    {
        if self.selected_obj.id == 0 {
            return;
        }
        match self.selected_obj.manip_area {
            ManipulationArea::None => { },
            ManipulationArea::Center => {
                self.move_object(rel_mouse_x, rel_mouse_y);
            },
            ManipulationArea::Rotation => {
                self.rotate_object(mouse_x, mouse_y);
            },
            ManipulationArea::UI => {},
            _ => { self.resize_object(mouse_x, mouse_y); }
        }
    }


    pub fn clear_selected(&mut self) {
        self.selected_obj.id = 0;        
        self.selected_obj.manip_area = ManipulationArea::None;
    }


    pub fn clear_selected_manip_area(&mut self) {
        self.selected_obj.manip_area = ManipulationArea::None;
    }


    pub fn selected_obj_rotating(&mut self) {
        self.selected_obj.manip_area = ManipulationArea::Rotation;
    }

    pub fn selected_obj_resizing(&mut self, area: ManipulationArea) {
        self.selected_obj.manip_area = area;
    }


    fn move_object(&mut self, rel_mouse_x: f32, rel_mouse_y: f32) {
        let obj_id = &self.selected_obj.id.clone();
        self.mark_updated(obj_id);

        let parent = self.object_parents.get(&self.selected_obj.id).expect(
            "Tried to move object, but no object with matching id found");
        let transformed_move =
            self.get_object_transform_basis(parent).invert().unwrap() *
            Vector2::new(rel_mouse_x, rel_mouse_y);

        let obj = self.objects_raw.get_mut(&self.selected_obj.id).expect(
            "Tried to move object, but no object with matching id found");
        obj.x_pos += transformed_move.x;
        obj.y_pos += transformed_move.y;
    }


    fn rotate_object(&mut self, mouse_x: f32, mouse_y: f32) {
        let obj_id = &self.selected_obj.id.clone();
        let (obj_x, obj_y) = self.get_object_pos(obj_id);
        let angle_to_mouse = f32::atan2(mouse_y - obj_y, mouse_x - obj_x) // - 90.0
            - f32::consts::PI / 2.0;
        self.update_rotation(obj_id, angle_to_mouse);
    }


    fn resize_object(&mut self, mouse_x: f32, mouse_y: f32) {
        let obj_id = self.selected_obj.id.clone(); 
        let manip_area = self.selected_obj.manip_area.clone();
        // println!("Resizing object on area {:?}", manip_area);
        let transform_mat = self.get_object_transform_basis(&obj_id);

        let (xdims, ydims, orig_width, orig_height) = {
            let raw = self.objects_raw.get(&obj_id).expect(
                "Tried to resize object, but no object with matching id found");
            // println!("When starting, width is {:?} and height is {:?} so hyp is {:?}", raw.width, raw.height, (raw.width.powi(2) + raw.height.powi(2)).sqrt());
            (transform_mat * Vector2::new(raw.width / 2.0, 0.0),
             transform_mat * Vector2::new(0.0, raw.height / 2.0),
             raw.width.clone(),
             raw.height.clone())
        };
        let (mut ax, mut ay) = self.get_object_pos(&obj_id);
        // println!("Old center point at ({:?}, {:?})", ax, ay);

        let (mut addx, mut addy) = match manip_area {
            ManipulationArea::NorthEast => (-1.0, -1.0),
            ManipulationArea::SouthEast => (-1.0, 1.0),
            ManipulationArea::NorthWest => (1.0, -1.0),
            ManipulationArea::SouthWest => (1.0, 1.0),
            ManipulationArea::North => (0.0, -1.0),
            ManipulationArea::South => (0.0, 1.0),
            ManipulationArea::West => (1.0, 0.0),
            ManipulationArea::East => (-1.0, 0.0),
            _ => (0.0, 0.0)
        };
        ax += addx * xdims.x + addy * ydims.x;
        ay += addx * xdims.y + addy * ydims.y;
        // println!("Calculated anchor at ({:?}, {:?})", ax, ay);

        let hyp_angle = f32::consts::PI
            - f32::abs(f32::atan2(mouse_y - ay, mouse_x - ax) -
                       f32::atan2(xdims.y, xdims.x));
        let hyp_len =
            ((mouse_y - ay).powi(2) + (mouse_x - ax).powi(2)).sqrt();
        // println!("Calculated hyp len to be {:?}", hyp_len);
        let mut width = (hyp_len * f32::cos(hyp_angle)).abs();
        let mut height = (hyp_len.powi(2) - width.powi(2)).sqrt();
        if addx == 0.0 { width = orig_width; }
        if addy == 0.0 { height = orig_height; }
        // println!("Calculated new width and height to be {:?} and {:?}",
        //          width, height);
        let xdims = transform_mat * Vector2::new(width / 2.0, 0.0);
        let ydims = transform_mat * Vector2::new(0.0, height / 2.0);
        addx = -addx; addy = -addy;
        let new_center_x = ax + addx * xdims.x + addy * ydims.x;
        let new_center_y = ay + addx * xdims.y + addy * ydims.y;
        // println!("Calculated new center point at ({:?}, {:?})",
        //          new_center_x, new_center_y);
        // println!("Mouse was always at ({:?}, {:?})", mouse_x, mouse_y);
        // println!("----------------------------------------");
        self.update(&obj_id, width, height, new_center_x, new_center_y);
    }


    pub fn draw_selected_obj_addons(&mut self) {
        let sel_id = self.selected_obj.id;
        let (hei, wid) = self.get_object_dims(&sel_id);
        let (hei, wid) = (hei / 2.0, wid / 2.0);
        let dist_to_pivot = hei + hei / 4.0;
        let pivot_square = f32::min(15.0, f32::min(hei, wid));

        // Rotation line and pivot
        self.add_specific_object(
            sel_id, ui::ROT_LINE, 0.0, dist_to_pivot / 2.0, ui::LEVEL_TOP1,
            2.0, dist_to_pivot, [0.0, 0.0, 0.0, 1.0], INVALID_TEXTURE, false);
        self.add_specific_object(
            sel_id, ui::ROT_PIVOT, 0.0, dist_to_pivot, ui::LEVEL_TOP2,
            pivot_square, pivot_square, [1.0, 0.65, 0.0, 1.0],
            INVALID_TEXTURE, true);

        // Boundary manip squares
        let sel_square = pivot_square * (0.7);
        self.add_specific_object(
            sel_id, ui::SEL_N, 0.0, hei, ui::LEVEL_TOP2,
            sel_square, sel_square, [0.5, 0.65, 0.4, 1.0], INVALID_TEXTURE, true);
        self.add_specific_object(
            sel_id, ui::SEL_S, 0.0, -hei, ui::LEVEL_TOP2,
            sel_square, sel_square, [0.5, 0.65, 0.4, 1.0], INVALID_TEXTURE, true);
        self.add_specific_object(
            sel_id, ui::SEL_E, wid, 0.0, ui::LEVEL_TOP2,
            sel_square, sel_square, [0.5, 0.65, 0.4, 1.0], INVALID_TEXTURE, true);
        self.add_specific_object(
            sel_id, ui::SEL_W, -wid, 0.0, ui::LEVEL_TOP2,
            sel_square, sel_square, [0.5, 0.65, 0.4, 1.0], INVALID_TEXTURE, true);
        self.add_specific_object(
            sel_id, ui::SEL_NE, wid, hei, ui::LEVEL_TOP2,
            sel_square, sel_square, [0.5, 0.65, 0.4, 1.0], INVALID_TEXTURE, true);
        self.add_specific_object(
            sel_id, ui::SEL_SE, wid, -hei, ui::LEVEL_TOP2,
            sel_square, sel_square, [0.5, 0.65, 0.4, 1.0], INVALID_TEXTURE, true);
        self.add_specific_object(
            sel_id, ui::SEL_SW, -wid, -hei, ui::LEVEL_TOP2,
            sel_square, sel_square, [0.5, 0.65, 0.4, 1.0], INVALID_TEXTURE, true);
        self.add_specific_object(
            sel_id, ui::SEL_NW, -wid, hei, ui::LEVEL_TOP2,
            sel_square, sel_square, [0.5, 0.65, 0.4, 1.0], INVALID_TEXTURE, true); 
    }


    pub fn update_selected_obj_addons(&mut self) {
        let sel_id = self.selected_obj.id;
        let (hei, wid) = self.get_object_dims(&sel_id);
        let (hei, wid) = (hei / 2.0, wid / 2.0);
        let dist_to_pivot = hei + hei / 4.0;
        let pivot_square = f32::min(15.0, f32::min(hei, wid));

        // Rotation line and pivot
        self.set_obj_attributes(
            &ui::ROT_LINE, 2.0, dist_to_pivot, 0.0, dist_to_pivot / 2.0);
        self.set_obj_attributes(
            &ui::ROT_PIVOT, pivot_square, pivot_square, 0.0, dist_to_pivot);

        // Boundary manip squares
        let sel_square = pivot_square * (0.7);
        self.set_obj_attributes(
            &ui::SEL_N, sel_square, sel_square, 0.0, hei);
        self.set_obj_attributes(
            &ui::SEL_S, sel_square, sel_square, 0.0, -hei);
        self.set_obj_attributes(
            &ui::SEL_E, sel_square, sel_square, wid, 0.0);
        self.set_obj_attributes(
            &ui::SEL_W, sel_square, sel_square, -wid, 0.0);
        self.set_obj_attributes(
            &ui::SEL_NE, sel_square, sel_square, wid, hei);
        self.set_obj_attributes(
            &ui::SEL_SE, sel_square, sel_square, wid, -hei);
        self.set_obj_attributes(
            &ui::SEL_SW, sel_square, sel_square, -wid, -hei);
        self.set_obj_attributes(
            &ui::SEL_NW, sel_square, sel_square, -wid, hei);
    }
}
