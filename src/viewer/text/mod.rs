////////// 3rd Party //////////
extern crate freetype as ft;
use self::ft::error::FtResult;
use self::ft::bitmap;
use viewer::gl;
use viewer::gl::types::*;
use viewer::cgmath::Matrix;
use viewer::cgmath::Matrix4;

////////// STD Lib //////////
use std::collections::HashMap;
use std::sync::{Arc, RwLock};
use self::ft::Face;
use std::ops::DerefMut;
use std::ffi::CString;

////////// My Libs //////////
use viewer::texture::Texture_Mapper;
use viewer::texture::Texture_ID;
use viewer::renderer::shape::Shape;
use viewer::renderer::shape::ShapeUniforms;
use viewer::object::controller::{ObjectController, ObjectID};

type Font_ID = u32;
pub const DEFAULT_FONT: u32 = 1;

//////////////////// PUB TYPES //////////////////// 

pub struct Text_Renderer
{
    text_objects: HashMap<u32, Text>,
    defined_fonts: HashMap<Font_ID, Font>,

    defined_glyphs: HashMap<Font_ID, HashMap<char, DefinedGlyph>>,
    texture_mapper: Arc<RwLock<Texture_Mapper<(u8, u8, u8, u8)>>>,
}

impl Text_Renderer
{
    /// Instantiates a Text_Renderer struct and preloads given characters inside of String.
    pub fn new(texture_mapper: Arc<RwLock<Texture_Mapper<(u8, u8, u8, u8)>>>,
               preload_glyphs: &String)
               -> Text_Renderer
    {
        // Initialize the default font
        let font = "./assets/Arial Unicode.ttf";
        let library = ft::Library::init().unwrap();
        let face = library.new_face(font, 0)
            .expect(format!("Font {} not found", font).as_str());
        // let face_w = 40;
        let pt_size = 14;
        let face_h = pt_size * 64; // 14pt font
        // The second two parameters are DPI --- find a way to gracefully calculate
        // The first parameter is width: if its 0, it indicates its same as height
        face.set_char_size(0, face_h as isize, 86, 86).unwrap();
        let font = Font { face: face, width: pt_size, height: pt_size };
        let mut fonts = HashMap::new();
        fonts.insert(DEFAULT_FONT, font);
        
        // Initialize glyph map
        let mut glyph_map = HashMap::new();
        glyph_map.insert(DEFAULT_FONT, HashMap::new());

        let mut renderer = Text_Renderer
        {
            text_objects: HashMap::new(),
            defined_fonts: fonts,
            defined_glyphs: glyph_map,
            texture_mapper: texture_mapper,
        };

        // Place wanted characters into the texture map
        for c in preload_glyphs.chars()
        {
            renderer.load_glyph(c, &DEFAULT_FONT);
        }

        renderer
    }
}

pub struct Text
{
    content: String, // optional
    font: Font_ID,
    content_to_object: Vec<u32>, // each character has its own object 
    // the entirety of the text is associated with a parent object
    associated_shape_object: u32, 
}

impl Text
{
    pub fn new(content: &String, font_id: Font_ID, associated_shape_object: u32)
               -> Text
    {
        Text
        {
            content: content.clone(),
            font: font_id,
            content_to_object: Vec::new(),
            associated_shape_object: associated_shape_object,
        }
    }
}

pub struct Font
{
    face: Face<'static>,
    width: i32,
    height: i32,
}

struct DefinedGlyph
{
    texture_id: Texture_ID,
    width: i32,
    height: i32,
    y_pos: f32,
    horizontal_spacing: f32,
}

impl DefinedGlyph
{
    pub fn new(c: char, texture: Texture_ID, width: i32, height: i32,
               font: &Font) -> DefinedGlyph
    {
        let font_height = font.height as f32;
        let y_pos = match c
        {
            'g'|'y'|'q'|'p'
                => { -font_height / 4.0 },
            'a'|'c'|'e'|'m'|'n'|'o'|'r'|'s'|'u'|'v'|'w'|'x'|'z'
                => { -font_height / 7.0 },
            'j' => { -font_height / 5.0 },
            't' => { -font_height / 14.0 },
            _ => { 0.0 },
        };

        let x_pos = 0.0;
        // let x_pos = match width as f32
        //     {
                
        //     };

        DefinedGlyph
        {
            texture_id: texture,
            width: width, height: height,
            y_pos: y_pos, horizontal_spacing: x_pos,
        }
    }
}

//////////////////// IMPL ////////////////////

impl Text_Renderer
{
    pub fn add_text(&mut self,
                    object_controller: &mut ObjectController, parent: u32,
                    y_pos: f32,  
                    content: &String, font_id: &Font_ID)
    {
        let chars_to_define: Vec<char> =
        {
            let defined_glyphs = self.defined_glyphs.get(font_id).unwrap();
            content.chars().filter(
                move |c|
                {
                    defined_glyphs.get(&c).is_none()
                }
            ).collect()
        };

        for c in chars_to_define
        {
            self.load_glyph(c, font_id);
        }

        let font = self.defined_fonts.get(font_id).unwrap();
        let glyphs = self.defined_glyphs.get(font_id).unwrap();
        
        let glyph_x_pos =
            (content.chars().count() as i32 * font.width as i32) / -2;
        let mut glyph_x_pos = glyph_x_pos as f32;

        for c in content.chars()
        {
            let glyph = glyphs.get(&c).unwrap();
            let y_pos: f32 = y_pos + glyph.y_pos;
            glyph_x_pos += glyph.width as f32 + glyph.horizontal_spacing;
            object_controller.add_generated_object(
                parent, glyph_x_pos as f32, y_pos, 1.0,
                glyph.width as f32, glyph.height as f32,
                glyph.texture_id, false
            );
        }
    }
    
    fn load_glyph(&mut self, character: char, font_id: &Font_ID) 
    {
        // Get font
        let font: &Font = self.defined_fonts.get(font_id)
            .expect("Could not find font with given Font_ID");
        let face = &font.face;

        // Get character texture from library 
        face.load_char(character as usize, ft::face::RENDER).unwrap();

        let glyph = face.glyph();
        let bitmap = glyph.bitmap();
        let width = bitmap.width() as usize;
        let height = bitmap.rows() as usize;

        let pixel_mode = bitmap.pixel_mode().unwrap();
            
        // Convert type of given pixel buffer to Vec of bytes
        let mut texture = Vec::new();
        let buffer = bitmap.buffer();

        use viewer::text::ft::bitmap::PixelMode;
        let pixel_extract: Box<Fn(usize) -> (u8, u8, u8, u8)> = 
            match pixel_mode
        {
            PixelMode::Gray => { Box::new(move |pos| {
                let val = 255 - buffer[pos];
                return (val, val, val, buffer[pos])
            }) },
            PixelMode::Lcd => { Box::new(move |pos| {
                return (buffer[pos], buffer[pos + 1], buffer[pos + 2], 255)
            }) },
            // PixelMode::Mono => { panic!("Got pixel mode Mono" ); },
            // PixelMode::Gray2 => { panic!("Got pixel mode Gray2" ); },
            // PixelMode::Gray4 => { panic!("Got pixel mode Gray4" ); },
            // PixelMode::LcdV => { panic!("Got pixel mode LcdV" ); },
            // PixelMode::Bgra => { panic!("Got pixel mode Bgra" ); },
            // PixelMode::None => { panic!("Got pixel mode None" ); },
            _ => { panic!("Got incompatible pixel mode in given font") },
        };

        // println!("Texture size is {} x {}", width, height);
        texture.reserve(width * height);
        for y in 0 .. height
        {
            for x in 0 .. width
            {
                let pos = (x + y * width) as usize; 
                // let tuple = (buffer[pos], buffer[pos + 1], buffer[pos + 2]);
                let tuple = pixel_extract(pos);
                texture.push(tuple);
            }
        }

        // Add texture to mapper
        let tex_id = self.texture_mapper.write().unwrap()
            .add_texture(texture, width as i32, height as i32);

        // Map tex_id for future use
        self.defined_glyphs.get_mut(font_id).unwrap()
            .insert(character,
                    DefinedGlyph::new(character, tex_id,
                                      width as i32, height as i32, font));
    }
}
