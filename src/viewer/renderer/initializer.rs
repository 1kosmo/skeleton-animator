use std;
use std::ffi::CString;
use std::path::Path;
use std::fs::File;
use std::io::Read;
use viewer::gl;
use viewer::gl::types::*;


fn create_shader(shader_type: GLenum, shader_f_path: &str) -> GLuint {
    let mut file = File::open(Path::new(shader_f_path)).ok()
        .expect("Could not find shader at specified location");
    let mut source: Vec<u8> = Vec::new();
    file.read_to_end(&mut source).unwrap();
    let c_str = CString::new(source).unwrap();
    let shader: GLuint;    
    unsafe {
        shader = gl::CreateShader(shader_type);
        gl::ShaderSource(shader, 1,
                         &c_str.as_ptr(),
                         std::ptr::null());
        gl::CompileShader(shader);

        let mut status: GLint = gl::TRUE as GLint;
        gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);
        if status == (gl::FALSE as GLint) {
            let mut info_log_length: GLint = 0;
            gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &mut info_log_length);

            let mut info_log = Vec::with_capacity(info_log_length as usize);
            info_log.set_len((info_log_length as usize) - 1);
            gl::GetShaderInfoLog(shader, info_log_length, std::ptr::null_mut(),
                                 info_log.as_mut_ptr() as *mut GLchar);

            let mut str_shader_type: &str = "";
            match shader_type {
                gl::VERTEX_SHADER => str_shader_type = "vertex",
                gl::GEOMETRY_SHADER => str_shader_type = "geometry",
                gl::FRAGMENT_SHADER => str_shader_type = "fragment",
                _ => {}
            }
            panic!("Compile failure in {} shader:\n{}", str_shader_type,
                   std::str::from_utf8(&info_log)
                   .ok().expect("Info log not in valid UTF-8"));
        }
    }
    shader
}


fn create_program(shader_list: &Vec<GLuint>) -> GLuint { unsafe {
    let program = gl::CreateProgram();

    for shader in shader_list {
        gl::AttachShader(program, *shader);
    }
    gl::LinkProgram(program);

    let mut status = gl::TRUE as GLint;
    gl::GetProgramiv(program, gl::LINK_STATUS, &mut status);
    if status == (gl::FALSE as GLint) {
        let mut info_log_length: GLint = 0;
        gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &mut info_log_length);

        let mut info_log = Vec::with_capacity(info_log_length as usize);
        info_log.set_len((info_log_length as usize) - 1);
        gl::GetProgramInfoLog(program, info_log_length, std::ptr::null_mut(),
                              info_log.as_mut_ptr() as *mut GLchar);
        panic!("Linker failure:\n{}", std::str::from_utf8(&info_log)
               .ok().expect("Info log not valid UTF-8"));
    }
    for shader in shader_list {
        gl::DetachShader(program, *shader);
    }

    program
} }


pub fn initialize_program(vert_shader: &str, frag_shader: &str)
                      -> GLuint { unsafe {
    let vs = create_shader(
        gl::VERTEX_SHADER, vert_shader);
    let fs = create_shader(
        gl::FRAGMENT_SHADER, frag_shader);
    let shader_program = create_program(&vec![vs, fs]);
    
    gl::DeleteShader(vs); gl::DeleteShader(fs);
    shader_program
} }
