////////// 3rd Party //////////
use viewer::gl;
use viewer::gl::types::*;

////////// STD Lib //////////
use std::collections::{HashMap, BinaryHeap, LinkedList};
use std::cmp::Ordering;
use std::marker::Copy;
use std;
use std::mem;
// use std::sync::{Arc, RwLock, RwLockWriteGuard, RwLockReadGuard};

const MAP_DIM: usize = 512;
const MAP_AREA: usize = MAP_DIM * MAP_DIM;

pub type Texture_ID = u32;
pub const INVALID_TEXTURE: Texture_ID = 0;

//////////////////// PUB TYPES ////////////////////

pub struct Texture_Mapper<T>
{
    texture_maps: Vec<Texture_Map<T>>,

    // pixel_size: u8,
    textures: HashMap<Texture_ID, Texture<T>>,
    // textures_to_map: BTreeMap<Texture_Size, Texture_ID>,
    textures_to_map: BinaryHeap<Texture_Size>,
    id_counter: Texture_ID,
    initialized: bool, 

    currently_bound_map: i32,
    currently_used_texture: Texture_ID,
    current_texture_vbo: GLuint,
}

impl <T> Texture_Mapper<T>
{
    pub fn new() -> Texture_Mapper<T>
    {
        Texture_Mapper
        {
            texture_maps: Vec::new(),
            
            textures: HashMap::new(),
            textures_to_map: BinaryHeap::new(),
            id_counter: 0,
            initialized: true,

            currently_bound_map: -1,
            currently_used_texture: INVALID_TEXTURE,
            current_texture_vbo: 0,
        }
    }
}

#[derive(Eq,Clone)]
struct Texture_Size
{
    width: i32,
    height: i32,    
    id: Texture_ID,
}

struct Texture<T>
{
    size: Texture_Size,
    pos_x: i32,
    pos_y: i32,

    map_number: usize,
    data: Vec<T>,
    // pixel_size: u8,
}

pub trait Pixel
{
    fn new_zero() -> Self;
    fn new1(p: u8) -> Self;
    fn new3(p1: u8, p2: u8, p3: u8) -> Self;
    fn new4(p1: u8, p2: u8, p3: u8, p4: u8) -> Self;
}

impl <T> Texture<T>
{
    pub fn new(size: Texture_Size, pos_x: i32, pos_y: i32, map_number: usize,
               data: Vec<T>)
               -> Texture<T>
    {
        Texture
        {
            size: size,
            pos_x: pos_x,
            pos_y: pos_y,
            map_number: map_number,
            data: data,
        }
    }
}

struct Texture_Map<T>
{
    data: Box<[T; MAP_AREA]>,
    gl_texture_name: GLuint,
    gl_name_initialized: bool,
    gl_in_memory: bool,
}

impl <T> Texture_Map<T>
{
    pub fn new(data: Box<[T; MAP_AREA]>) -> Texture_Map<T>
    {
        Texture_Map { data: data, gl_texture_name: 0,
                      gl_name_initialized: false, gl_in_memory: false }
    }
}

//////////////////// IMPL ////////////////////

impl <T> Texture_Mapper<T> where T: Pixel + Copy
{
    /// Adds a texture to a texture map. This forces the Texture_Mapper to
    /// reconstruct its maps on next texture retrieval.
    ///
    /// The ID returned is used to retrieve texture.
    pub fn add_texture(&mut self, texture: Vec<T>, width: i32, height: i32)
                       -> Texture_ID
    {
        // Check texture fits width and size parameters
        assert!(texture.len() == ((width * height) as usize));

        self.id_counter += 1;
        // Create entries
        let size =
            Texture_Size { width: width, height: height, id: self.id_counter };
        let new_texture: Texture<T> = Texture::new(size.clone(), 0, 0, 0, texture);

        // Insert info structure
        self.textures.insert(self.id_counter, new_texture);
        // Insert to map that keeps order in terms of size
        self.textures_to_map.push(size);
        
        self.initialized = false;

        return self.id_counter;
    }

    /// Initialization to texture mapper that has to be done after global
    /// OpenGL context has been set.
    pub fn init_after_opengl_context_created(&mut self)
    { unsafe {
        gl::GenBuffers(1, &mut self.current_texture_vbo);
    } }

    /// Constructs full texture maps if there are unmapped textures present.
    pub fn prepare_for_use(&mut self)
    {
        if self.initialized
        {
            return
        }

        self.currently_bound_map = -1;
        self.currently_used_texture = INVALID_TEXTURE;

        self.map_unmapped();

        self.initialized = true;
    }

    /// Fills in the information for where each texture is supposed to be stored on
    /// the maps.
    fn map_unmapped(&mut self)
    {
        // This represents a point at which a rectangle can be inserted into the map
        // The rectangle is bounded by end_x or/and end_y;
        struct Entry_Pos
        {
            s_x: i32,
            s_y: i32,
            e_x: i32,
            e_y: i32,

            map_number: usize,
        }

        impl Entry_Pos
        {
            pub fn new(s_x: i32, s_y: i32, e_x: i32, e_y: i32, map: usize)
                       -> Entry_Pos
            {
                Entry_Pos {s_x: s_x, s_y: s_y, e_x: e_x, e_y: e_y, map_number: map}
            }
            
            pub fn width(&self) -> i32 { self.e_x - self.s_x }
            pub fn height(&self) -> i32 { self.e_y - self.s_y }
        }

        let mut available_spots: LinkedList<Entry_Pos> = LinkedList::new();
        let mut maps_used = 0; 
        available_spots.push_back(
            Entry_Pos::new(0, 0, MAP_DIM as i32, MAP_DIM as i32, maps_used));


        for size in self.textures_to_map.iter()
        {
            let mut chosen = 0;
            for spot in available_spots.iter()
            {
                if (size.width <= spot.width()) && (size.height <= spot.height())
                {
                    break;
                }
                chosen += 1;
            }

            // If no spot suitable spot exists, create a new spot on a new map
            if available_spots.len() == chosen
            {
                maps_used += 1;
                available_spots.push_back(
                    Entry_Pos::new(0, 0, MAP_DIM as i32, MAP_DIM as i32, maps_used));
            }

            // Split linked list into two -- second half will have chosen at start
            let mut spots_after_chosen = available_spots.split_off(chosen);
            
            let updated_x; let updated_y; let chosen_map;
            let prev_x; let prev_y;
            let mut remove_extended_chosen_spot: bool = false;
            let mut new_smaller_spot: Entry_Pos;
            {
                let mut spots_after_chosen = spots_after_chosen.iter_mut();
                let mut chosen_spot = spots_after_chosen.next().unwrap();
                prev_x = chosen_spot.s_x;
                prev_y = chosen_spot.s_y;

                updated_y = chosen_spot.s_y + size.height;
                updated_x = chosen_spot.s_x;
                chosen_map = chosen_spot.map_number;

                new_smaller_spot = Entry_Pos::new(prev_x, updated_y,
                                                  chosen_spot.e_x, chosen_spot.e_y,
                                                  // chosen_spot.s_x + size.width, chosen_spot.e_y,
                                                  chosen_map);
                // Modify current spot for future entries
                chosen_spot.s_x += size.width;
                // let new_extended_chosen_x = chosen_spot.s_x;

                // // If the end of the spot (chosen_spot.s_x + width) exceeds
                // //    s_x of the spot after it (i.e. the spot just below it), remove
                // {
                //     let one_after_chosen = spots_after_chosen.next();
                //     remove_extended_chosen_spot =
                //         one_after_chosen.is_some() &&
                //         (new_extended_chosen_x >= one_after_chosen.unwrap().s_x);
                // }
            }

            // if remove_extended_chosen_spot
            // {
            //     spots_after_chosen.pop_front();
            // }

            // Modify spots before this one because they might be block horizontally
            for spot in available_spots.iter_mut().rev()
            {
                if chosen_map != spot.map_number { break }
                
                if spot.s_y < updated_y && spot.e_x > updated_x
                {
                    spot.e_x = updated_x;
                }
            }
            // Modify spots after this one because they might have a ceiling now
            for spot in spots_after_chosen.iter_mut().next()
            {
                if chosen_map != spot.map_number { break }
                
                if spot.e_y > prev_y && spot.s_x < updated_x
                {
                    spot.e_y = prev_y;
                }
            }           
            // Check if limits can be lifted off the new spot (if it has x limit)
            if MAP_DIM > new_smaller_spot.e_x as usize
            {
                new_smaller_spot.e_x = MAP_DIM as i32;
                for spot in spots_after_chosen.iter_mut().next()
                {
                    if chosen_map != spot.map_number { break }
                    
                    if spot.s_y > new_smaller_spot.s_y
                    {
                        new_smaller_spot.e_x = spot.s_x;
                        break;
                    }
                }                   
            }

            // Place new (smaller) spot to be placed between the two halves
            //    if 
            available_spots.push_back(new_smaller_spot);
            available_spots.append(&mut spots_after_chosen);

            // Write where texture is to be located
            if let Some(texture) = self.textures.get_mut(&size.id)
            {
                texture.map_number = chosen_map;
                texture.pos_x = prev_x;
                texture.pos_y = prev_y;
            }
        }
        self.construct_maps();
    } // End fn map_unmapped

    /// Populates the arrays within Texture_Mapper.texture_maps
    fn construct_maps(&mut self)
    {
        // Invalidate current maps first
        for map in &mut self.texture_maps
        {
            map.invalidate();
        }

        for tex_size in self.textures_to_map.iter()
        {
            let tex_id = tex_size.id;
            if let Some(texture) = self.textures.get(&tex_id)
            {
                // Check that there are enough maps to fit the texture
                if self.texture_maps.len() <= texture.map_number
                {
                    let cur_size = self.texture_maps.len();
                    // self.texture_maps.reserve(texture.map_number - cur_size);
                    if texture.map_number >= cur_size
                    {
                        for _ in cur_size .. (texture.map_number + 1)
                        {
                            self.texture_maps.push(Texture_Map::new(unsafe {
                                Box::from_raw(vec![T::new_zero(); MAP_AREA]
                                              .into_boxed_slice().as_mut_ptr()
                                              as * mut _
                                )}
                            ));
                        }
                    }
                }
                
                // Insert the texture
                let ref mut map = self.texture_maps.get_mut(texture.map_number)
                    .unwrap().data;
                for y in 0 .. texture.size.height
                {
                    for x in 0 .. texture.size.width
                    {
                        let cur_pixel = (x + (y * texture.size.width)) as usize;
                        let map_y = MAP_DIM - (texture.pos_y + texture.size.height - y) as usize;
                        let pixel_on_map =
                            (x + texture.pos_x) as usize + 
                            (map_y * MAP_DIM);
                        map[pixel_on_map] = texture.data[cur_pixel];
                    } // end y for-loop 
                } // end x for-loop 
            } // end Some(texture) for-loop
        } // end tex_id for-loop
    } // end fn construct_maps
}

impl Texture_Mapper<(u8, u8, u8, u8)>
{
    /// Loads texture into OpenGL through TexImage2D
    pub fn use_texture(&mut self, texture_id: &Texture_ID,
                       vao: &GLuint, texture_data_unif: &GLint)
    {
        if self.currently_used_texture == *texture_id
        {
            set_texture_attrib_pointers(vao, &self.current_texture_vbo);
            return
        }
        let texture = self.textures.get(texture_id).unwrap();
        let width = texture.size.width; let height = texture.size.height;
        let texture_coordinates: [GLfloat; 12] = {
            let left_x = texture.pos_x as f32 / MAP_DIM as f32;
            let right_x = (texture.pos_x + width) as f32 / MAP_DIM as f32;
            let top_y = 1.0 - ((texture.pos_y + height)as f32 / MAP_DIM as f32);
            let bottom_y = 1.0 - (texture.pos_y as f32 / MAP_DIM as f32);
            // let left_x = 0.0;
            // let right_x = 13.0 / 128.0;
            // let top_y = 60.0 / 128.0;
            // let bottom_y = 128.0;
            // let left_x = 0.0;
            // let right_x = 1.0;
            // let top_y = 0.0;
            // let bottom_y = 1.0;
            // let top_y = (MAP_DIM as i32 - texture.pos_y) as f32 / MAP_DIM as f32;
            // let bottom_y =
            //     (MAP_DIM as i32 - texture.pos_y - height) as f32 / MAP_DIM as f32;
            [
                right_x, top_y,
                right_x, bottom_y,
                left_x, top_y,
                left_x, top_y,
                right_x, bottom_y,
                left_x, bottom_y
            ]
        };
            
        set_vertex_buffer(
            self.current_texture_vbo, &texture_coordinates,
            (texture_coordinates.len() * mem::size_of::<GLfloat>()) as GLsizeiptr);
        set_texture_attrib_pointers(vao, &self.current_texture_vbo);

        // Bind texture map if its not bound
        if (self.currently_bound_map as usize) != texture.map_number
        { unsafe {
            let texture_map: &mut Texture_Map<(u8, u8, u8, u8)> =
                self.texture_maps.get_mut(texture.map_number).unwrap();
            // Generate OpenGL texture name if necessary
            if !texture_map.gl_name_initialized
            {
                gl::GenTextures(1, &mut texture_map.gl_texture_name);
                texture_map.gl_name_initialized = true;
            }

            gl::Uniform1i(*texture_data_unif, 0);
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, texture_map.gl_texture_name);
            
            // Load texture data to OpenGL if it still hasn't been done
            if !texture_map.gl_in_memory
            {
                gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA8 as GLint,
                               MAP_DIM as i32, MAP_DIM as i32, // width and height
                               0, gl::RGBA, gl::UNSIGNED_BYTE,
                               mem::transmute(&texture_map.data[0]));
                texture_map.gl_in_memory = true;
            }
            // TODO: Upon destruction of texture map, call glDeleteTextures
        } }
    }
    
    pub fn make_test_map_file(&self)
    {
        use std::path::Path;
        use std::fs::File;
        // use std::io::prelude::*;
        use std::io::{Write, BufWriter};

        let i = 0;
        for map_obj in &self.texture_maps
        {
            let file = File::create(Path::new(format!("./test_map{}.txt", i).as_str())).unwrap();
            let mut file = BufWriter::new(file);

            let ref map = map_obj.data;
            let newline = "\n".as_bytes();
            for y in 0 .. MAP_DIM {
                for x in 0 .. MAP_DIM {
                    let val = map[y * MAP_DIM + x];
                    let sym0 = match val.0 {
                        v if v == 255 => " ",
                        v if v > 127 => "*",
                        _ => "+"
                    };
                    file.write(sym0.as_bytes()).expect("Unable to write test map");
                }
                file.write(newline).expect("Could not write newline");
            }
        }
    }
}

////////// UTILITIES //////////

fn set_vertex_buffer<T>(vbo: GLuint, vertices: &[T],
                     vert_size: GLsizeiptr) 
{ unsafe {
    let vertices: *const std::os::raw::c_void = mem::transmute(&vertices[0]);
    gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
    gl::BufferData(gl::ARRAY_BUFFER, vert_size, vertices, gl::STATIC_DRAW);
    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
} }

fn set_texture_attrib_pointers(vao: &GLuint, vbo: &GLuint) 
{ unsafe {
    // gl::BindVertexArray(*vao);

    gl::BindBuffer(gl::ARRAY_BUFFER, *vbo);
    // gl::EnableVertexAttribArray(1); 
    // 1 is bound to "tex_coords"
    gl::VertexAttribPointer(1, 2, gl::FLOAT, gl::FALSE, 0, std::ptr::null());

    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
    // gl::BindVertexArray(0);
} }

impl Ord for Texture_Size
{
    fn cmp(&self, other: &Texture_Size) -> Ordering
    {
        let area1 = self.width * self.height;
        let area2 = other.width * other.height;
        if area1 > area2
        {
            Ordering::Greater
        }
        else if area1 < area2
        {
            Ordering::Less
        }
        else
        {
            Ordering::Equal
        }
    }
}

impl PartialOrd for Texture_Size {
    fn partial_cmp(&self, other: &Texture_Size) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Texture_Size {
    fn eq(&self, other: &Texture_Size) -> bool {
        // self.width == other.width && self.height == other.height
        self == other
    }
}

impl Pixel for (u8, u8, u8)
{
    fn new_zero() -> Self
    {
        (0, 0, 0)
    }

    fn new1(p: u8) -> Self
    {
        (p, p, p)
    }

    fn new3(p1: u8, p2: u8, p3: u8) -> Self
    {
        (p1, p2, p3)
    }

    fn new4(p1: u8, p2: u8, p3: u8, p4: u8) -> Self
    {
        (p1, p2, p3)
    }
}

impl Pixel for (u8, u8, u8, u8)
{
    fn new_zero() -> Self
    {
        (0, 0, 0, 0)
    }

    fn new1(p: u8) -> Self
    {
        (p, p, p, 1)
    }

    fn new3(p1: u8, p2: u8, p3: u8) -> Self
    {
        (p1, p2, p3, 1)
    }

    fn new4(p1: u8, p2: u8, p3: u8, p4: u8) -> Self
    {
        (p1, p2, p3, p4)
    }
}

impl <T> Texture_Map<T>
{
    pub fn invalidate(&mut self)
    {
        self.gl_in_memory = false;
        if self.gl_name_initialized
        { unsafe {
            gl::DeleteTextures(1, &self.gl_texture_name);
        } }
    }
}

impl <T> Drop for Texture_Map<T>
{
    fn drop(&mut self)
    {
        self.invalidate();
    }
}
