use viewer::gl;
use viewer::sdl2;
use viewer::sdl2::video::GLProfile;


pub fn set_up_context(win_width: u32, win_height: u32)
                  -> (sdl2::Sdl, sdl2::video::Window, sdl2::video::GLContext) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let gl_attributes = video_subsystem.gl_attr();

    gl_attributes.set_context_profile(GLProfile::Core);
    gl_attributes.set_context_flags().debug().set();
    gl_attributes.set_context_version(3, 3);
    gl_attributes.set_double_buffer(true);

    let window = video_subsystem.window("Skeleton Animator",
                                        win_width, win_height)
        .opengl()
        .build().unwrap();
    assert_eq!(gl_attributes.context_profile(), GLProfile::Core);
    assert_eq!(gl_attributes.context_version(), (3, 3));
    let gl_ctx = window.gl_create_context().unwrap();
    window.gl_make_current(&gl_ctx).unwrap();

    gl::load_with(|name| video_subsystem.gl_get_proc_address(name) as *const _);

    (sdl_context, window, gl_ctx)
}

