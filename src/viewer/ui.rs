////////// 3rd Party //////////
use viewer::sdl2::mouse::Cursor;
use viewer::sdl2::mouse::SystemCursor;

////////// My libs //////////
use viewer::object::ManipulationArea;
use viewer::Viewer;
use viewer::ActionState;
use viewer::WaitingState;
use viewer::texture::{INVALID_TEXTURE};

pub const SRFC_BTN: u32 = 3;
pub const BTN_LINK_OBJ: u32 = 4;

pub const SRFC_COLOR_PICKER: u32 = 15;
pub const SLIDER_RED: u32 = 16;
pub const SLIDER_GREEN: u32 = 17;
pub const SLIDER_BLUE: u32 = 18;
pub const BTN_COLOR_PICKER: u32 = 19;

pub const ROT_LINE: u32 = 20;
pub const ROT_PIVOT: u32 = 21;
pub const SEL_N: u32 = 22;
pub const SEL_W: u32 = 23;
pub const SEL_S: u32 = 24;
pub const SEL_E: u32 = 25;
pub const SEL_NW: u32 = 26;
pub const SEL_SE: u32 = 27;
pub const SEL_SW: u32 = 28;
pub const SEL_NE: u32 = 29;

pub const MAX_UI_ID: u32 = 30;

pub const LEVEL_SRFC1: f32 = 0.9;
pub const LEVEL_SRFC2: f32 = 0.91;
pub const LEVEL_BUTTON: f32 = 0.95;
pub const LEVEL_TOP1: f32 = 0.98;
pub const LEVEL_TOP2: f32 = 0.99;
// const LEVEL_TOP3: f32 = 1.0;


impl Viewer {
    pub fn adjust_cursor(&mut self, area: ManipulationArea) {
        let mut new_cursor = match area {
            ManipulationArea::Center => { SystemCursor::SizeAll },  
            ManipulationArea::UI => {
                match self.focus_object {
                    SEL_N ... SEL_NE => { SystemCursor::Crosshair },
                    _ => SystemCursor::Arrow,
                }
            }
            _ => { SystemCursor::Arrow }
        };
        
        if new_cursor != self.cursor_type {
            self.cursor_type = new_cursor;
            self.cursor = Cursor::from_system(self.cursor_type).unwrap();
            self.cursor.set();
        }
    }

    
    pub fn select_object(&mut self) {
        self.object_controller.draw_selected_obj_addons();
    }


    pub fn deselect_object(&mut self) {
        self.delete_object(ROT_LINE);
        self.delete_object(ROT_PIVOT);
        self.delete_object(SEL_N);
        self.delete_object(SEL_S);
        self.delete_object(SEL_E);
        self.delete_object(SEL_W);
        self.delete_object(SEL_NE);
        self.delete_object(SEL_NW);
        self.delete_object(SEL_SE);
        self.delete_object(SEL_SW);
    }


    pub fn clicked_ui(&mut self, ui_id: u32) {
        println!("CLICKED UI {:?}", ui_id);
        match ui_id {
            ROT_PIVOT => {
                self.focus_state = ActionState::Rotate;
                self.object_controller.selected_obj_rotating();
            },
            BTN_LINK_OBJ => {
                self.waiting_state = WaitingState::Link;
            },
            SEL_N ... SEL_NE => {
                self.focus_state = ActionState::Resize;
                match ui_id {
                    SEL_N => { self.object_controller
                               .selected_obj_resizing(ManipulationArea::North) },
                    SEL_S => { self.object_controller
                               .selected_obj_resizing(ManipulationArea::South) },
                    SEL_E => { self.object_controller
                               .selected_obj_resizing(ManipulationArea::East) },
                    SEL_W => { self.object_controller
                               .selected_obj_resizing(ManipulationArea::West) },
                    SEL_NE => {
                        self.object_controller
                            .selected_obj_resizing(ManipulationArea::NorthEast) },
                    SEL_NW => {
                        self.object_controller
                            .selected_obj_resizing(ManipulationArea::NorthWest) },
                    SEL_SE => {
                        self.object_controller
                            .selected_obj_resizing(ManipulationArea::SouthEast) },
                    SEL_SW => {
                        self.object_controller
                            .selected_obj_resizing(ManipulationArea::SouthWest) },
                    _ => {},
                }
            }
            _ => {},
        }
    }


    pub fn initialize_ui(&mut self) {
        let but_h = 90.0;
        let but_w = 90.0;
        let margin = 5.0;

        // Draw button underlay
        self.object_controller.add_specific_object(
            0, SRFC_BTN,
            -(self.win_width as f32 / 2.0) + but_w / 2.0 + margin, 0.0,
            LEVEL_SRFC1, but_w + margin * 2.0, self.win_height as f32,
            [0.8, 0.8, 0.8, 1.0], INVALID_TEXTURE, false);

        /**** Draw Buttons ****/
        // Link objects button
        self.object_controller.add_specific_object(
            SRFC_BTN, BTN_LINK_OBJ,
            0.0, (self.win_height as f32 / 2.0) - but_h / 2.0 - margin,
            LEVEL_BUTTON, but_w, but_h, [0.5, 0.5, 0.0, 1.0],
            INVALID_TEXTURE, true);

        /**** Draw Color Picker ****/
        // Underlay
        self.object_controller.add_specific_object(
            SRFC_BTN, SRFC_COLOR_PICKER,
            0.0, -(self.win_height as f32 / 2.0) + but_h,
            LEVEL_SRFC2, but_w, but_h * 2.0, [0.3, 0.3, 0.3, 1.0],
            INVALID_TEXTURE, false);
        // Sliders
        self.object_controller.add_specific_object(
            SRFC_COLOR_PICKER, SLIDER_RED,
            -but_w / 3.0, -but_h + 25.0, LEVEL_BUTTON, but_w / 3.0 - margin, 20.0,
            [1.0, 0.0, 0.0, 1.0], INVALID_TEXTURE, true);
        self.object_controller.add_specific_object(
            SRFC_COLOR_PICKER, SLIDER_GREEN,
            0.0, -but_h + 25.0, LEVEL_BUTTON, but_w / 3.0 - margin, 20.0,
            [0.0, 1.0, 0.0, 1.0], INVALID_TEXTURE, true);
        self.object_controller.add_specific_object(
            SRFC_COLOR_PICKER, SLIDER_BLUE,
            but_w / 3.0, -but_h + 25.0, LEVEL_BUTTON, but_w / 3.0 - margin, 20.0,
            [0.0, 0.0, 1.0, 1.0], INVALID_TEXTURE, true);
        // Choose color button
        self.object_controller.add_specific_object(
            SRFC_COLOR_PICKER, BTN_COLOR_PICKER,
            0.0, -but_h, LEVEL_BUTTON, but_w / 2.0, 20.0, [0.0, 0.0, 0.0, 1.0],
            INVALID_TEXTURE, true);
    }
}
