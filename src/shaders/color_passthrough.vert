#version 330

layout(location = 0) in vec2 position;
layout(location = 1) in vec3 color;

smooth out vec4 the_color;

uniform mat4 camera_to_clip_matrix;
uniform mat4 model_to_camera_matrix;

void main() {
  vec4 camera_pos = model_to_camera_matrix * vec4(position, -1.0, 1.0);
  gl_Position = camera_to_clip_matrix * camera_pos;
  the_color = vec4(color, 1.0);
}
