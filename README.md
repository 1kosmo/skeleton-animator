# skeleton-animator

To run: 'cargo run' inside root of project.

Work in progress functionality:
- Click on empty space and drag to create square.
- Click on square and drag to move.
- Click on square to select and allow resizing and rotation.
- Select square, click on top-left button and click another square to attach the first as child of the second.
